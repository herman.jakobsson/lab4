package no.uib.inf101.gridview;
import javax.swing.JFrame;
import java.awt.Color;

import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
    /**
     * @param args
     */
    public static void main(String[] args) {
        // Opprett et ColorGrid-objekt med 3 rader og 4 kolonner
        ColorGrid colorGrid = new ColorGrid(3, 4);
        
        // Sett farger i hjørnene som beskrevet
        colorGrid.setColorAt(0, 0, Color.RED); // Rød
        colorGrid.setColorAt(0, 3, Color.BLUE); // Blå
        colorGrid.setColorAt(2, 0, Color.YELLOW); // Gul
        colorGrid.setColorAt(2, 3, Color.GREEN); // Grønn
        
        // Opprett en GridView med det opprettede ColorGrid-objektet
        GridView gridView = new GridView(colorGrid);
        
        // Opprett og konfigurer et JFrame-vindu
        JFrame frame = new JFrame();
        frame.setContentPane(gridView);
        frame.setTitle("Grid View");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

