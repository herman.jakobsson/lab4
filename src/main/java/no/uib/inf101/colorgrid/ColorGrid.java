package no.uib.inf101.colorgrid;


import java.util.List;
import java.awt.Color;
import java.util.ArrayList;


public class ColorGrid implements IColorGrid{
    int rows;
    int cols;
    Color[][] grid;

    public ColorGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new Color [rows][cols];
    }

    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }

    @Override
    public List<CellColor> getCells() {
        ArrayList<CellColor> newList = new ArrayList<>();

        for(int row=0; row<this.grid.length; row++){
            for(int col=0; col<this.grid[row].length; col++){
                newList.add(new CellColor(new CellPosition(row, col), this.grid[row][col]));
            }
        }



        return newList;
    }


    @Override
    public Color get(CellPosition pos) {
        return this.grid[pos.row()][pos.col()];


    }

    @Override
    public Color set(CellPosition pos, Color color) {
        return this.grid[pos.row()][pos.col()] = color;
    }

    public void setColorAt(int i, int j, Color red) {
    }

}
